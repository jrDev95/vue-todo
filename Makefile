node-start:
	podman run -it --rm -d --workdir=/opt/  -v $(CURDIR):/opt/:z --name node -p 3000:3000 -p 3001:3001 docker.io/library/node:14-alpine
node-stop:
	podman stop node
vue-ui-start:
	npx vue ui -p 3001 -H 0.0.0.0
